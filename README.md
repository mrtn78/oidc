Project to define a profile for OpenID Connect, for usage in the Dutch governmental/public domain.

# Mandetory Dutch governmental standard

The profile has become a formal standard as of 21-09-2023. Links to the standard, repo and working versions are listed in the table:

| Formal standaard                                            | Published version                                         | Working  version                                                  | Repository                                                   |
| ------------------------------------------------------------ | ------------------------------------------------------------ | ------------------------------------------------------------ | ------------------------------------------------------------ |
| 🏛️ [NLGov OpenID Connect profile (OIDC)](https://www.forumstandaardisatie.nl/open-standaarden/authenticatie-standaarden) | [OIDC v1.0.1 (definitief)](https://gitdocumentatie.logius.nl/publicatie/api/oidc/) | [OIDC (werkversie)](https://logius-standaarden.github.io/OIDC-NLGOV/) | [OIDC-NLGOV](https://github.com/Logius-standaarden/OIDC-NLGOV) |


# Supporting documentation
Documentation and resulting reports of the Working Group can be found in the [Wiki](https://gitlab.com/logius/oidc/-/wikis/Verslaglegging-werkgroep).

# OpenID Foundation iGov profile for OpenID Connect
Original upstream OpenID Foundation iGov profile for OpenID Connect (as well as for OAuth2) can be found on [https://bitbucket.org/openid/igov/src/master/](https://bitbucket.org/openid/igov/src/master/).
The markdown (.md) version was created using [html2text](https://alir3z4.github.io/html2text), based on the HTML version of upstream iGov profile.
